const express = require('express')
const cors = require('cors')
const app = express()

app.use(express.json())
app.use(cors())

app.listen(3000, () => {
    console.log('Servidor corriendo en el puerto 3000')
})

let ciudades = ['La Paz', 'Cochabamba', 'Santa Cruz', 'Beni', 'Pando']

app.get('/ciudades', (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)))

let misDestinos = []
app.get('/my', (req, res, next) => {
    res.json(misDestinos)
})

app.post('/my', (req, res, next) => {
    console.log(req.body)
    misDestinos.push(req.body.nuevo)
    res.json(misDestinos)
})

app.get('/api/translation', (req, res, next) => res.json([{
    lang: req.query.lang,
    key: 'Hola',
    value: 'Hola ' + req.query.lang
}]))