import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { Store } from '@ngrx/store';
// import { AppState } from 'src/app/app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';

// class DestinoApiClientViejo{
//   getById(id : string) : DestinoViaje{
//     console.log('llamando por clase vieja')
//     return null
//   }
// }

// interface AppConfig{
//   apiEndPoint : string
// }

// const APP_CONFIG_VALUE : AppConfig = {
//   apiEndPoint : 'mi_api.com'
// }

// const APP_CONFIG = new InjectionToken<AppConfig>('app.congif')

// class DestinoApiClientDecorated extends DestinosApiClient{
//   constructor(@Inject(APP_CONFIG) private config : AppConfig, store : Store<AppState>){
//     super(store)
//   }

//   getById(id : string) : DestinoViaje{
//     console.log('LLamado por la clase decorada')
//     console.log('config : ' + this.config.apiEndPoint)
//     return super.getById(id)
//   }
// }

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers : [
    DestinosApiClient
  ]
  // providers : [DestinosApiClient, {provide : DestinoApiClientViejo, useExisting : DestinosApiClient}]
  // providers : [
  //   {provide : APP_CONFIG, useValue : APP_CONFIG_VALUE},
  //   {provide : DestinosApiClient, useClass : DestinoApiClientDecorated},
  //   {provide : DestinoApiClientViejo, useExisting : DestinosApiClient}
  // ]
})
export class DestinoDetalleComponent implements OnInit {

  destino : DestinoViaje

  constructor( private route : ActivatedRoute,
    private destinosApiClient : DestinosApiClient ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.params['id']
    this.destino = this.destinosApiClient.getById(id)
  }

}
