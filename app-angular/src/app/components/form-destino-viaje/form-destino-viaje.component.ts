import {
  Component,
  EventEmitter,
  forwardRef,
  Inject,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { fromEvent } from 'rxjs';
import { DestinoViaje } from '../../models/destino-viaje.model';
import {
  map,
  filter,
  distinctUntilChanged,
  switchMap,
  debounceTime,
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css'],
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 2;
  SearchResult: string[];

  constructor(
    private fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = this.fb.group({
      nombre: [
        '',
        Validators.compose([
          Validators.required,
          this.nombreValidatorParametrizable(this.minLongitud),
        ]),
      ],
      url: ['', Validators.required],
    });
  }

  ngOnInit() {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    // fromEvent(elemNombre, 'input')
    //   .pipe(
    //     map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
    //     filter((text) => text.length > 4),
    //     debounceTime(200),
    //     distinctUntilChanged(),
    //     switchMap(() => ajax('/assets/datos.json'))
    //   )
    //   .subscribe((ajaxResponse) => {
    //     this.SearchResult = ajaxResponse.response;
    //   });

    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter((text) => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text : string) => ajax(this.config.apiEndPoint+'ciudades?q='+text))
      ).subscribe(ajaxResponse => this.SearchResult = ajaxResponse.response)
  }

  guardar(nombre: string, url: string) {
    const destino = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(destino);
  }

  nombreValidator(control: FormControl): { [key: string]: boolean } {
    const longitud = control.value.toString().trim().length;
    if (longitud > 0 && longitud < 5) {
      return { minLongNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < minLong) {
        return { minLongNombre: true };
      }
      return null;
    };
  }
}
