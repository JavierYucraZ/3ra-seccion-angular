import { TestBed } from '@angular/core/testing';

import { UsuarioLoggeadoGuard } from './usuario-loggeado.guard';

describe('UsuarioLoggeadoGuard', () => {
  let guard: UsuarioLoggeadoGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UsuarioLoggeadoGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
