import {v4 as uuid} from 'uuid';

export class DestinoViaje {

    public selected : boolean
    public servicios : string[]
    public votes : number

    nombre : string
    imagen : string
    id = uuid();

    constructor( nombre : string, imagen : string){
        this.nombre = nombre
        this.imagen = imagen
        this.votes = 0
        this.servicios = ['piscina', 'desayuno']
    }

    isSelected(){
        return this.selected
    }

    setSelected(valor : boolean){
        this.selected = valor
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}